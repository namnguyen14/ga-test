const chunk = require('./chunk')
const unique = require('./unique')
const fitness = require('./fitness')
function crossover(selection) {
    let child = []
    for (mama = 0; mama <= selection.length; mama++) {
        if (selection[mama] == undefined)
            continue
        let mamaGen = selection[mama].pop
        let hMamaGen = chunk.func(mamaGen, mamaGen.length / 2)
        for (let papa = mama + 1; papa <= selection.length; papa++) {
            if (selection[papa] == undefined)
                continue
            let papaGen = selection[papa].pop
            let hPapaGen = chunk.func(papaGen, papaGen.length / 2)
            let crossGen = hMamaGen[0].concat(hPapaGen[1]).filter(unique.func) // ghep gen cha me
            // console.log(crossGen)
            let veziGutChild = fitness.fitness(crossGen)
            if (veziGutChild.code == 200)
                child.push(veziGutChild)
        }
    }

    return child;
}
module.exports.func = crossover