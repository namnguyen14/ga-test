let pop = require('./generatePop') //  tao quan the
let fitness = require('./fitness') // tinh fitness
const crossover = require('./crossover') // lai cheo
const in_array = require('./inArray')
const resultSave = require('./resultSave')
let notVulPath = []
let vulPath = []
let crossoverArr = []
for (let i = 0; i <= 50; i++) {
    let selection = pop.log()
    if (in_array.func(selection.length, [7, 8, 9]) != undefined) {
        let fitNessPop = fitness.fitness(selection)
        if (fitNessPop.code !== 500) {
            if (fitNessPop.code == 200) {
                notVulPath.push(fitNessPop)
            } else if (fitNessPop.fitness > 3) {
                crossoverArr.push(fitNessPop)
            } else if (fitNessPop.code == 300) {
                vulPath.push(fitNessPop)
            }
        }
    }
}
if (crossoverArr.length > 1) {
    if (crossover.func(crossoverArr).length > 0)
        console.log('crossover child', crossover.func(crossoverArr))
        resultSave.func(crossover.func(crossoverArr), 'crossoverChild')
}
console.log('not vulnerable path', notVulPath)
resultSave.func(notVulPath, 'notVulPath')
console.log('vulnerable path', vulPath)
resultSave.func(vulPath, 'vulPath')

