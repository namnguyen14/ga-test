const in_array = require('./inArray')
const feasibleCheck = require('./feasibleCheck')
const desiredPath = [6,7,12,14,15]
function fitness(selection) {
    let fitness = 0
    let defaultRes = {
        code: 500,
        msg: 'invalid path',
        pop: [],
        fitness: fitness
    }
    for(let i = 0; i <= selection.length; i ++){
        if(in_array.func(selection[i], desiredPath)){
            fitness += 1;
        }
    }
    if(fitness >= 5){
        let feasiblePath = feasibleCheck.func(selection)
        defaultRes = {
            code: 200,
            msg: feasiblePath == 'yes' ? 'not vulnerable path' : 'infeasible path',
            pop: selection,
            fitness: fitness,
            
        }
    }else {
        defaultRes = {
            code: 300,
            msg: 'vulnerable path',
            pop: selection,
            fitness: fitness
        }
    }
    return defaultRes
}
module.exports.fitness = fitness