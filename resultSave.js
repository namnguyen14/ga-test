var fs = require('fs');
const fnc = (result, filename) => {
    const time = new Date()
    const suffix = Date.parse(time)
    fs.writeFile(`./result/${filename}-${suffix}.txt`, JSON.stringify(result, null, 2), function(err) {
    if (err) {
        console.log(err);
    }
});
}
module.exports.func = fnc