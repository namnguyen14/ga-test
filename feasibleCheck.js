const in_array = require('./inArray')
const check = (path) => {
    if(in_array.func(8, path) || in_array.func(9, path)){
        if(in_array.func(10, path)){
            return 'no'
        }
        if(in_array.func(11, path)){
            return 'no'
        }
        if(!in_array.func(9, path)){
            return 'no'
        }
        if(!in_array.func(8, path)){
            return 'no'
        }
    }
    if(in_array.func(10, path) || in_array.func(11, path)){
        if(in_array.func(8, path)){
            return 'no'
        }
        if(in_array.func(9, path)){
            return 'no'
        }
        if(!in_array.func(11, path)){
            return 'no'
        }
        if(!in_array.func(10, path)){
            return 'no'
        }
    }
    return !in_array.func(8, path) || ! !in_array.func(10, path) ? 'no' :'yes'
}
module.exports.func = check