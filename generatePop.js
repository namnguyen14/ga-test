
let path = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
let randomLength = [7, 8, 9]
function generate() {
    let result = []
    const randomPathLength = randomLength[Math.floor(Math.random() * randomLength.length)]
    for (let i = 0; i < randomPathLength; i++) {
        const randomElement = path[Math.floor(Math.random() * path.length)]
        let removeItem = path.indexOf(randomElement)
        path.splice(removeItem, 1)
        result.push(randomElement)
    }
    Object.keys(result).forEach(key => result[key] === undefined ? delete result[key] : {})
    path = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    return result.sort(function (a, b) { return a - b });
}
module.exports.log = generate
module.exports.path = path
// let popArr = []
// console.log(vulPath.length)
// let pop = generatePopulation();
// for (let i = 0; i<vulPath.length; i++){
//     console.log(i)
//     // console.log(pop)
//     // popArr[i] = popArr.push(pop)
// }
// console.log(popArr)
